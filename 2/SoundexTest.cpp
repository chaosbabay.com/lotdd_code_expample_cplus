#include <iostream>
using namespace std;

#include "gmock/gmock.h"
#include "Soundex.h"

using namespace testing;

//using ::testing::Eq;

class SoundexEncoding : public Test{
    public:
        Soundex soundex;
};


TEST_F(SoundexEncoding, RetainsSoleLetterOfOneLetterWord) {


   ASSERT_THAT(soundex.encode("A"),Eq("A000"));
}

TEST_F(SoundexEncoding,PadsWithZerosToEnsureThreeDigits){

    ASSERT_THAT(soundex.encode("I"),Eq("I000"));

}

TEST_F(SoundexEncoding,ReplacesConsonantsWithAppropriateDigits){


   ASSERT_THAT(soundex.encode("Ax"),Eq("A200"));
}

TEST_F(SoundexEncoding,IgnoresNonAlphabetics){

    ASSERT_THAT(soundex.encode("A#"),Eq("A000"));

}

TEST_F(SoundexEncoding,ReplacesMutipleConsonantWithDigits){
    ASSERT_THAT(soundex.encode("Acdl"),Eq("A234"));
}

TEST_F(SoundexEncoding,LimitsLengthToFourCharacters){
    ASSERT_THAT(soundex.encode("Dcdlb").length(),Eq(4u));
}


TEST_F(SoundexEncoding,IgnoreVowelLikeLetters){
    ASSERT_THAT(soundex.encode("BaAeEiIoOuUhHyYcdl"),Eq("B234"));
}

TEST_F(SoundexEncoding,CombinesDuplicateEncodings){

    ASSERT_THAT(soundex.encodeDigit('b'),Eq(soundex.encodeDigit('f')));
    ASSERT_THAT(soundex.encodeDigit('c'),Eq(soundex.encodeDigit('g')));
    ASSERT_THAT(soundex.encodeDigit('d'),Eq(soundex.encodeDigit('t')));

    ASSERT_THAT(soundex.encode("Abfcgdt"),Eq("A123"));
}

TEST_F(SoundexEncoding,UppercasesFirstLetter){
    ASSERT_THAT(soundex.encode("abcd"),StartsWith("A"));
}

TEST_F(SoundexEncoding,IgnoresCaseWhenEncodeingConsonants){
    ASSERT_THAT(soundex.encode("BCDL"),Eq(soundex.encode("Bcdl")));
}

TEST_F(SoundexEncoding,combinesDuplicateCodesWhen2ndLetterDuplicates1st){
    ASSERT_THAT(soundex.encode("Bbcd"),Eq("B230"));
}

TEST_F(SoundexEncoding,DoesNotCotCombineDuplicateEncodingsSeparatedByVowels){
    ASSERT_THAT(soundex.encode("Jbob"),Eq("J110"));
}
